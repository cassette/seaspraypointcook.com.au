<?php

require 'class/CassetteEnquire.php';

$name = $_POST['name']; // REQUIRED
$surname = $_POST['surname'];
$email = $_POST['email'];
$postcode = $_POST['postcode'];
$phone = $_POST['phone'];
$comment = $_POST['comments'];
$select = $_POST['select'];
$other = $_POST['other'];

$msg = 'Name: '.$name.' '.$surname."\n".
    'Email: '.$email."\n".
    'Postcode: '.$postcode."\n".
    'Contact Phone Number: '.$phone."\n".
    'How did you hear about us?: '.$select."\n".
    'Other:'.$other."\n".
    'Additional Comments: '.$comment."\n";

if (!$email)
    exit("error : no email received");


$cassetteEnquire = new CassetteEnquire();
$data = array();
$errors = array();



// EMAIL SETUP
$data['email'] = array();
$data['email']['senderName'] = $name; // REQUIRED
$data['email']['senderEmail'] = $email; // REQUIRED
$data['email']['subject'] = 'Message from the seaspraypointcook.com.au Website'; // REQUIRED
$data['email']['body'] = $msg;

// EMAIL RECIPIENTS
// EXAMPLE
$data['email']['recipients'] = array();
//$data['email']['recipients'][] = array('seaspray@savills.com.au', 'normal'); // REQUIRED
//$data['email']['recipients'][] = array('lora@cassette.com.au', 'bcc');
//$data['email']['recipients'][] = array('it@cassette.com.au', 'bcc');
$data['email']['recipients'][] = array('digital@cassette.com.au', 'normal'); // REQUIRED

$errors[] = $cassetteEnquire->Email($data['email']);





if($select == 'Other'){
    $select = 'Other: '. urlencode($other) .'';
}

$url = '';
// for testing
//'http://residentialenquiries.savills.com.au/enquiry.aspx?&propertyid=7538&fname=' . urlencode($name) . '&sname=' . urlencode($surname) .'&email=' . urlencode($email) . '&mobile='. urlencode($phone) . '&postocde=' . urlencode($postcode) . '&comments=' . urlencode($comment) . '&heardfrom=' . urlencode($select) . '&redirect=';



httpGet($url);
function httpGet($url)
{
    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
//  curl_setopt($ch,CURLOPT_HEADER, false);

    $output=curl_exec($ch);

    curl_close($ch);
    return $output;
}


if ($cassetteEnquire->error_flag){
    print_r(json_encode(array("error" => true, "data" => $errors)));

}
else{
print_r(json_encode(array("error" => false, "data" => "")));

}
?>