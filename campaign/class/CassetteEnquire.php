<?php
/**
 * Created by PhpStorm.
 * User: cassette
 * Date: 18/07/2014
 * Time: 11:06 AM
 */


require 'vendors/phpmailer/PHPMailerAutoload.php';
require 'vendors/MyLogPHP-1.2.1.class.php';
include_once(__DIR__ . '/../CassetteEnquireConfig.php');


class CassetteEnquire {

    private $logger;
    private $mailer;
    private $logMe;
    public  $error_flag;


    function __construct(){

        // LOGGER INIT
        $this->error_flag = false;
        $this->logger = new MyLogPHP($_SERVER['DOCUMENT_ROOT']."/logs/".LOGFILE);
        // MAIL INIT
        $this->mailer = new PHPMailer;
        $this->logMe = LOGGING;


    }

    private function  buildReturn($type,$module, $data = ""){
        if ($type == "error")
            $this->error_flag = true;
        return array('result' => $type, "module" => $module, "data" => $data);
    }


    function log($logType, $fromModule, $data, $error = ""){
        switch ($logType) {
            case "error":
                $this->logger->error("[".$fromModule."] [".$data. "] [". $error."]");
                break;
            case "info":
                $this->logger->info("[".$fromModule."] [".$data. ']');
                break;
        }
    }


    function CampaignMonitor($data, $logMe = true){
        if (!isset($data['url']))
            return array('result' => "error", "module" => "campaignMonitor", "data" => "URL Missing");
        $url = $data['url'];
        $extraFields = $data['fields'];
        $dataToSend = array();
        foreach($extraFields as $name => $value)
        {
            if ($value)
                $dataToSend[] = $name."=" . trim($value);
        }
        $stringToSend = implode("&", $dataToSend);
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $stringToSend);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec( $ch );
        if(curl_errno($ch))
        {
            if ($this->logMe)
                $this->log("error", "campaignMonitor", $stringToSend, "Error Signing up");
            return $this->buildReturn("error", "campaignMonitor", "Error When Signing up");
        }
        if ($this->logMe)
            $this->log("info", "campaignMonitor", $stringToSend);

        return $this->buildReturn("success", "CampaignMonitor");
    }

    function MailChimp(){

    }


    function Email($data, $logMe = true){

        $this->mailer->FromName = $data['senderName'];
        $this->mailer->From = $data['senderEmail'];
        $this->mailer->Subject = $data['subject'];
        $this->mailer->Body    = $data['body'];

        if ($data['recipients']){
            foreach ($data['recipients'] as $recipient)
            {
                $email = $recipient[0];
                $type = $recipient[1];
                switch ($type) {
                    case "bcc":
                        $this->mailer->addBCC($email);
                        break;
                    case "cc":
                        $this->mailer->addCC($email);
                        break;
                    default:
                        $this->mailer->addAddress($email);     // Add a recipient
                        break;
                }

            }
        }

        if(!$this->mailer->send()) {
            if ($logMe)
                $this->log("error", "Mail", $data['body'], $this->mailer->ErrorInfo);
            return $this->buildReturn("error", "mail", $this->mailer->ErrorInfo);
        }
        if ($logMe)
            $this->log("info", "Mail", $data['body']);
        return $this->buildReturn("success", "mail");
    }
}